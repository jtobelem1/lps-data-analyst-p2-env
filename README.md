<img src="img/logo.svg" alt="logo" height="100"/>

# Environnement technique pour la formation Simplon - data analyst

Vous pouvez trouver ici les instructions pour installer et tester les outils utilisés pour la formation de data analyst.
Certains sont à installer sur votre ordinateur (tableau, environnement python) et pour les autres, vous devez tester la connexion (en particulier à travers le proxy d'entreprise).


# ![local](img/desktop.png) Installations locales


## Tableau

> Tableau est a installer **localement**, voici la procédure :

- [Download the latest version of Tableau Desktop and Tableau Prep Builder here](https://www.tableau.com/tft/activation)
- Click on the link above and select “Download Tableau Desktop” and “Download Tableau Prep Builder”. On the form, enter your school email address for Business E-mail and enter the name of your school for Organization.
- Activate with your product key:  TCPP-FDFF-AE00-B64A-DF68 
- Already have a copy of Tableau Desktop installed? Update your license in the application: Help menu → Manage Product Keys

## Python

> L'environnement python est a installer **localement**, voici la procédure :

### Installation

#### Télécharger le repo :

<https://gitlab.com/jtobelem1/lps-data-analyst-p2-env/-/archive/master/lps-data-analyst-p2-env-master.zip>

Ensuite vous pouvez décompresser l'archive et aller dans le dossier créé.


#### Installation de conda :
- https://docs.anaconda.com/anaconda/install/windows/ (**recommandé pour les utilisateurs de windows**)
- [OU] https://docs.conda.io/en/latest/miniconda.html

#### Installation de mamba (plus rapide que conda)

*Commande à exécuter depuis le conda prompt pour les utilisateurs de windows (ainsi que les suivantes).*
```
conda install -n base -c conda-forge mamba
```

#### Créer l'environnement à partir du fichier (pour update un env existant, voir ci-dessous)

Reperez le chemin pour aller dans le dossier décompressé : 

![path](/img/path.png)

Puis collez ce chemin dans anaconda prompt avec la commande cd (change directory) :

![path](/img/cd.png)



Ensuite executez la commande suivante :

```
mamba env create -f environment.yml
```

#### [OU] Updater l'environnement à partir du fichier

Executez la commande suivante :

```
mamba env update -f environment.yml
```

#### Activer the environment
```
conda activate simplon-data-analyst
```

#### Lancer jupyter lab
```
jupyter lab
```

### Lancer les notebooks

> Exécuter toutes les cellules de tous les notebooks, il ne doit y avoir aucune erreur. Le résultat attendu après l'exécution se trouve dans le dossier pdf.

- test-sklearn-seaborn
- test-db-connection
- test-install-extra-dep
- test-graphviz
- test-prophet
- test-xgboost


# ![server](img/server.png) Accès aux ressources distantes

## Dataiku DSS

> Dataiku DSS est installé sur une **machine distante**, voici comment se connecter à cette machine :

- http://simplon-formations-data.westeurope.cloudapp.azure.com/

login : check-user
mot de passe : check

## Pgadmin

> Pgadmin est installé sur une **machine distante**, voici comment se connecter à cette machine :

- http://simplon-formations-data.westeurope.cloudapp.azure.com/pgadmin4

login : check@simplon.co
mot de passe : check-user


## Environnement de secours python

> L'environnement de secours python est installé sur une **machine distante**, voici comment se connecter à cette machine :

- http://simplon-formations-data.westeurope.cloudapp.azure.com/jupyter

mot de passe : simplon.co

Important : **Travaillez uniquement dans le dossier qui porte votre nom dans le dossier workspaces**

Une fois la connexion à l'environnement lancée, vous pouvez tester tous les notebooks, et vérifier avec le résultat attendu du dossier pdf.

> Il est possible aussi d'utiliser une machine limitée gratuite avec l'environnement préconfiguré sur mybinder en cliquant sur l'icône ci-dessous (peut-être long à lancer la 1ère fois) :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jtobelem1%2Flps-data-analyst-p2-env/HEAD?urlpath=lab)

Depuis cet environnement, vous pouvez créer des notebooks et faire des tests.

